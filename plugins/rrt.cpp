//
// Created by Shrijit Singh on 2020-02-03.
//


#include "global_planner/rrt.h"

#include <pluginlib/class_list_macros.hpp>

PLUGINLIB_EXPORT_CLASS(rrt::RRT, global_planner::GlobalPlanner)

#include <math.h>
#include <float.h>


using namespace std;

void rrt::RRT::initialize_plugin(rclcpp::Node::SharedPtr node) {
  this->node = node;
  load_params();
}

void rrt::RRT::load_params() {
  node->declare_parameter("max_vertices");
  node->declare_parameter("max_increment");

  auto parameters_client = std::make_shared<rclcpp::SyncParametersClient>(node);
  while (!parameters_client->wait_for_service(1s)) {

    if (!rclcpp::ok()) {
      RCLCPP_ERROR(node->get_logger(), "Interrupted while waiting for the service. Exiting.");
      rclcpp::shutdown();
    }
    RCLCPP_INFO(node->get_logger(), "service not available, waiting again...");
  }

  auto set_parameters_results = parameters_client->set_parameters(
      {
          rclcpp::Parameter("max_vertices", 2000),
          rclcpp::Parameter("max_increment", 5),
      });

  for (auto & result : set_parameters_results) {
    if (!result.successful) {
      RCLCPP_ERROR(node->get_logger(), "Failed to set parameter: %s", result.reason.c_str());
    }
  }

  max_vertices_ = parameters_client->get_parameter<int>("max_vertices");
  max_increment_ = parameters_client->get_parameter<int>("max_increment");
}

nav_msgs::msg::Path rrt::RRT::create_path_to_pose(const nav_msgs::msg::OccupancyGrid::SharedPtr map, const geometry_msgs::msg::PoseStamped goal) {
  std::random_device dev;
  rng_ = new std::mt19937(std::random_device()());

  search_points_publisher_ = this->node->create_publisher<visualization_msgs::msg::Marker>("search_points", 10);
  search_lines_publisher_ = this->node->create_publisher<visualization_msgs::msg::Marker>("search_lines", 10);
  path_publisher_ = this->node->create_publisher<nav_msgs::msg::Path>("path", 10);
  map_grid_ = MapGrid::convert_occupancy_grid_to_map_grid(map);
  this->set_current(0, 0); // TODO: Get current position from TF
  this->compute_path(map, goal);
  nav_msgs::msg::Path path;
  return path;
}

void rrt::RRT::set_goal(geometry_msgs::msg::PoseStamped goal) {
  goal_ = boost::add_vertex(Vertex{(int) -goal.pose.position.x, (int) -goal.pose.position.y}, graph_);
}

void rrt::RRT::set_current(int x, int y) {
  current_ = boost::add_vertex(Vertex{(int) x, (int) y}, graph_);

}

inline rrt::vertex_t rrt::RRT::gen_rand_vertex() {
    std::uniform_int_distribution<int> pos_x(0, map_grid_->width - 1);
    std::uniform_int_distribution<int> pos_y(0, map_grid_->height - 1);

    double dist_x = pos_x(*rng_), dist_y = pos_y(*rng_);

    if (map_grid_->cell_at(dist_x, dist_y).cost < LETHAL_COST)
      return boost::add_vertex(Vertex{(int) dist_x, (int) dist_y}, graph_);
    else
      return gen_rand_vertex();
}

inline double rrt::RRT::euclid_dist(vertex_t u, vertex_t v) {
  return std::sqrt(std::pow(graph_[u].x - graph_[v].x, 2) + std::pow(graph_[u].y - graph_[v].y, 2));
}

inline std::pair<rrt::vertex_t, double> rrt::RRT::closest_vertex(vertex_t v) {
  boost::graph_traits <graph_t>::vertex_iterator v_iter, end;
  double min_dist = DBL_MAX;
  vertex_t closest_vertex = 0;
  for (boost::tie(v_iter, end) = vertices(graph_); v_iter != end; ++v_iter) {
    double dist = euclid_dist(*v_iter, v);
    if (dist < min_dist && *v_iter != v) {
      min_dist = dist;
      closest_vertex = *v_iter;
    }
  }

  return std::make_pair(closest_vertex, min_dist);
}

bool rrt::RRT::bresenham(vertex_t u, vertex_t v) {
  int y1, y2, x1, x2;

  x1 = graph_[u].x;
  y1 = graph_[u].y;
  y2 = graph_[v].y;
  x2 = graph_[v].x;

  bool swapped = false;

  if (std::abs(x2 - x1) < std::abs(y2 - y1)) {
    swap(x1, y1);
    swap(x2, y2);
    swapped = true;
  }

  if (x1 > x2) {
    swap(x1, x2);
    swap(y1, y2);
  }

  int dx = abs(x2-x1);
  int sx = x1<x2 ? 1 : -1;
  int dy = -1 * abs(y2-y1);
  int sy = y1<y2 ? 1 : -1;
  int err = dx+dy;  /* error value e_xy */

  while (true) {

    if (!swapped && map_grid_->cell_at(x1, y1).cost >= LETHAL_COST) return false;
    else if (map_grid_->cell_at(y1, x1).cost >= LETHAL_COST) return false;

    if (x1==x2 && y1==y2) break;

    int e2 = 2*err;
    if (e2 >= dy) {
      err += dy;
      x1 += sx;
    }
    if (e2 <= dx) {
      err += dx;
      y1 += sy;
    }
  }

  return true;
}

void rrt::RRT::compute_path(const nav_msgs::msg::OccupancyGrid::SharedPtr map, const geometry_msgs::msg::PoseStamped goal) {

  for (int k = 0; k < max_vertices_; ++k) {
    auto qrand = gen_rand_vertex();
    auto cv = closest_vertex(qrand);

    if (!bresenham(qrand, cv.first)) {
      boost::remove_vertex(qrand, graph_);
      continue;
    }

    if (cv.second >= max_increment_) {
      boost::remove_vertex(qrand, graph_);
      int x = graph_[cv.first].x - (max_increment_ * (graph_[cv.first].x - graph_[qrand].x)) / cv.second;
      int y = graph_[cv.first].y - (max_increment_ * (graph_[cv.first].y - graph_[qrand].y)) / cv.second;
      qrand = boost::add_vertex(Vertex{x, y}, graph_);
    }

    boost::add_edge(qrand, cv.first, Edge{cv.second}, graph_);
//
//    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  this->set_goal(goal);
  auto cv = closest_vertex(goal_);
  if (!bresenham(goal_, cv.first)) {
    boost::remove_vertex(goal_, graph_);
    RCLCPP_INFO(node->get_logger(), "No Path Found!");
  } else {
    boost::add_edge(goal_, cv.first, Edge{cv.second}, graph_);
    publish_search();
    publish_path();
  }



  graph_.clear();
}

void rrt::RRT::publish_path() {
  nav_msgs::msg::Path path;
  path.header.frame_id = "map";
  path.header.stamp = node->now();


  auto indexmap = boost::get(boost::vertex_index, graph_);
  auto colormap = boost::make_vector_property_map<boost::default_color_type>(indexmap);

  MyVisitor vis;
  boost::depth_first_search(graph_, vis, colormap, goal_);

  std::vector<vertex_t> vctr = vis.GetVector();

  for (auto u : vctr) {
    geometry_msgs::msg::PoseStamped p;
    p.header.frame_id = "map";
    p.pose.position.x = -graph_[u].x;
    p.pose.position.y = -graph_[u].y;

    path.poses.push_back(p);
    if (u == current_) break;
  }

  path_publisher_->publish(path);
}

void rrt::RRT::publish_search() {
  visualization_msgs::msg::Marker msg;
  msg.header.frame_id = "map";
  msg.header.stamp = node->now();
  msg.ns = "rrt";
  msg.id = 0;
  msg.type = visualization_msgs::msg::Marker::POINTS;
  msg.scale.x = 0.2;
  msg.scale.y = 0.2;
  msg.color.g = 1.0;
  msg.color.a = 1.0;

  boost::graph_traits <graph_t>::vertex_iterator i, endv;
  for (boost::tie(i, endv) = vertices(graph_); i != endv; ++i) {
    geometry_msgs::msg::Point p;
    p.x = graph_[*i].x;
    p.y = graph_[*i].y;
    p.z = 0.0;
    msg.points.push_back(p);
  }


  visualization_msgs::msg::Marker msg_line;
  msg_line.header.frame_id = "map";
  msg_line.header.stamp = node->now();
  msg_line.ns = "rrt";
  msg_line.id = 1;
  msg_line.type = visualization_msgs::msg::Marker::LINE_LIST;
  msg_line.scale.x = 0.1;
  msg_line.scale.y = 0.1;
  msg_line.color.r = 1.0;
  msg_line.color.a = 1.0;

  typedef boost::graph_traits<graph_t>::edge_iterator EdgeIterator;
  typedef std::pair<EdgeIterator, EdgeIterator> EdgePair;
  typedef boost::graph_traits<graph_t>::vertex_descriptor VertexDescriptor;

  EdgePair ep;
  VertexDescriptor u,v;
  for (ep = edges(graph_); ep.first != ep.second; ++ep.first)
  {
    u = source(*ep.first, graph_);
    v = target(*ep.first, graph_);

    geometry_msgs::msg::Point p1, p2;
    p1.x = graph_[u].x;
    p1.y = graph_[u].y;
    p1.z = 0.0;
    p2.x = graph_[v].x;
    p2.y = graph_[v].y;
    p2.z = 0.0;

    msg_line.points.push_back(p1);
    msg_line.points.push_back(p2);
  }


  search_points_publisher_->publish(msg);
  search_lines_publisher_->publish(msg_line);
}

