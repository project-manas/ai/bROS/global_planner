//
// Created by Shrijit Singh on 2018-12-03.
//

#include "global_planner/dijkstra.h"

#include <pluginlib/class_list_macros.hpp>

PLUGINLIB_EXPORT_CLASS(dijkstra::Dijkstra, global_planner::GlobalPlanner)

#include <math.h>

using namespace std;

void dijkstra::Dijkstra::initialize_plugin(rclcpp::Node::SharedPtr node) {
  this->node = node;
  potential_publisher_ = this->node->create_publisher<nav_msgs::msg::OccupancyGrid>("potential", 10);
}

nav_msgs::msg::Path dijkstra::Dijkstra::create_path_to_pose(const nav_msgs::msg::OccupancyGrid::SharedPtr map, const geometry_msgs::msg::PoseStamped goal) {
  this->map = MapGrid::convert_occupancy_grid_to_map_grid(map);
  this->set_current(70, 40); //TODO: Get current position from TF
  this->set_goal(goal);
  this->allow_unknown_ = true;
  this->smoothen_path_ = false;
  this->compute_path(map, goal);
  return *(this->path);
}

void dijkstra::Dijkstra::compute_path(const nav_msgs::msg::OccupancyGrid::SharedPtr map, const geometry_msgs::msg::PoseStamped goal) {
  this->calculate_potential();
  this->path = this->create_path();
}

void dijkstra::Dijkstra::set_goal(geometry_msgs::msg::PoseStamped goal) {
  this->goal = &this->map->data[(int) goal.pose.position.x][(int) goal.pose.position.y];
}

void dijkstra::Dijkstra::set_current(int x, int y) {
  this->current = &this->map->data[x][y];
  this->current->distance = 0;
  this->current->predecessor = NULL;
}

void dijkstra::Dijkstra::calculate_potential() {
  this->unvisited_cells_.push(this->current);

  while (!unvisited_cells_.empty()){
    MapCell *currentCell = unvisited_cells_.top();
    this->unvisited_cells_.pop();
    currentCell->visited = true;
    if (currentCell == this->goal) break;

    int i = currentCell->x, j = currentCell->y;
    if (currentCell->x - 1 > 0){
      update_cell(&map->data[i - 1][j], currentCell);
    }
    if (currentCell->x + 1 < this->map->width){
      update_cell(&map->data[i + 1][j], currentCell);
    }
    if (currentCell->y - 1 > 0){
      update_cell(&map->data[i][j - 1], currentCell);
    }
    if (currentCell->y + 1 < this->map->height){
      update_cell(&map->data[i][j + 1], currentCell);
    }
    if (currentCell->x - 1 > 0 && currentCell->y - 1 > 0){
      update_cell(&map->data[i - 1][j - 1], currentCell);
    }
    if (currentCell->x - 1 > 0 && currentCell->y + 1 < this->map->height){
      update_cell(&map->data[i - 1][j + 1], currentCell);
    }
    if (currentCell->x + 1 < this->map->width && currentCell->y - 1 > 0){
      update_cell(&map->data[i + 1][j - 1], currentCell);
    }
    if (currentCell->x + 1 < this->map->width && currentCell->y + 1 < this->map->height){
      update_cell(&map->data[i + 1][j + 1], currentCell);
    }
  }

  this->publish_potential();
}

double euclidean(MapCell *c1, MapCell *c2) {
  return pow(pow((c1->x - c2->x), 2) + pow((c1->y - c2->y), 2), 0.5);
}

bool dijkstra::Dijkstra::los(MapCell *c1, MapCell *c2, double &trav_cost) {
  int x0 = c1->x, y0 = c1->y, x1 = c2->x, y1 = c2->y;
  int dy = y1- y0, dx =x1 - x0, f = 0;
  int sx, sy;

  if (dy < 0) {
    dy = -dy;
    sy = -1;
  } else {
    sy = 1;
  }

  if (dx < 0) {
    dx = -dx;
    sx = -1;
  } else {
    sx = 1;
  }

  if (dx >= dy) {
    while (x0 != x1) {
      f = f + dy;
      if (f >= dx) {
        if (map->data[y0 + (sy - 1) / 2][x0 + (sx - 1) / 2].cost >= LETHAL_COST)
          return false;
        y0 += sy;
        f -= dx;
      }
      if (f != 0 && map->data[y0 + (sy - 1) / 2][x0 + (sx - 1) / 2].cost >= LETHAL_COST)
        return false;
      if (dy == 0 && map->data[y0 + (sy - 1) / 2][x0].cost >= LETHAL_COST && map->data[y0 + (sy - 1) / 2][x0 - 1].cost >= LETHAL_COST)
        return false;
      x0 += sx;
    }
  } else {
    while (y0 != y1) {
      f = f + dx;
      if (f >= dy) {
        if (this->map->data[y0 + (sy - 1) / 2][x0 + (sx - 1) / 2].cost >= LETHAL_COST)
          return false;
        x0 += sx;
        f -= dy;
      }
      if (f != 0 && this->map->data[y0 + (sy - 1) / 2][x0 + (sx - 1) / 2].cost >= LETHAL_COST)
        return false;
      if (dx == 0 && this->map->data[y0][x0 + (sx - 1) / 2].cost >= LETHAL_COST && this->map->data[y0 - 1][y0 + (sx - 1) / 2].cost >= LETHAL_COST)
        return false;
      y0 += sy;
    }
  }

  return true;
}

void dijkstra::Dijkstra::update_cell(MapCell *cell, MapCell *current){
  if (cell->cost < LETHAL_COST && !cell->visited) {
    double trav_cost = 0.0;
    bool has_los = false;

    if (smoothen_path_)
      has_los = los(current->predecessor, cell, trav_cost);

    if (has_los) {
      if (current->predecessor->distance + euclidean(current->predecessor, cell) < cell->distance) {
        cell->distance = current->predecessor->distance + euclidean(current->predecessor, cell);
        cell->predecessor = current->predecessor;
        this->unvisited_cells_.push(cell);
      }
    } else {
      if (current->distance +  cell->cost + 1 < cell->distance) {
        cell->distance = current->distance + cell->cost + 1;
        cell->predecessor = current;
        this->unvisited_cells_.push(cell);
      }
    }
  }
}


void dijkstra::Dijkstra::publish_potential() {
  nav_msgs::msg::OccupancyGrid potential;

  potential.header.frame_id = "map";
  potential.header.stamp = this->node->now();
  potential.info.resolution = this->map->resolution;
  potential.info.height = this->map->height;
  potential.info.width = this->map->width;
  potential.info.origin.position.x = 0.0;
  potential.info.origin.position.y = 0.0;
  potential.info.origin.position.z = 0.0;
  potential.info.origin.orientation.x = 0.70711;
  potential.info.origin.orientation.y = 0.70711;
  potential.info.origin.orientation.z = 0.0;
  potential.info.origin.orientation.w = 0.0;
  double max = -1;
  potential.data.resize(this->map->width * this->map->height);
  for (int i = 0; i < this->map->height * this->map->width; ++i) {
    if (this->map->data[0][i].distance >= 1000000) {
      potential.data[i] = -1;
      continue;
    }
    else {
      if (this->map->data[0][i].distance  > max)
        max = this->map->data[0][i].distance;
    }

//    (1.0 / (x - min + 1.0))
//    potential.data[i] = ((float) this->map->data[0][i].distance / (1.414 * this->map->width)) * 255;
    potential.data[i] = 100 * this->map->data[0][i].distance / max;
//    potential.data[i] = 1 * this->map->data[0][i].cost;
  }

  this->potential_publisher_->publish(potential);
}

nav_msgs::msg::Path *dijkstra::Dijkstra::create_path() {
  MapCell *path_cell = this->goal;
  auto *path = new nav_msgs::msg::Path();
  vector<geometry_msgs::msg::PoseStamped> poses;
  path->header.frame_id = "map";
  while (path_cell != this->current){
    geometry_msgs::msg::PoseStamped position;
    position.header.frame_id = "map";
    position.pose.position.x = path_cell->x;
    position.pose.position.y = path_cell->y;
    poses.push_back(position);
    path_cell = path_cell->predecessor;
  }
  path->poses.resize(poses.size());
  for (int i = 0; i < poses.size(); ++i) {
    path->poses[i] = poses[i];
  }
  return path;
}


