//
// Created by Shrijit Singh on 2018-12-06.
//

#include "global_planner/global_planner_ros.h"

using namespace std::chrono_literals;

global_planner::GlobalPlannerROS::GlobalPlannerROS() : Node("global_planner_node"), plugin_loader_("global_planner", "global_planner::GlobalPlanner") {
  plugin_ = plugin_loader_.createSharedInstance("rrt::RRT");
  plugin_->create_global_planner((rclcpp::Node::SharedPtr) this, plugin_);
}

  void global_planner::GlobalPlannerROS::load_parameters() {
    this->parameters_client_->wait_for_service(1s);
    while (!this->parameters_client_->wait_for_service(1s)) {
      if (!rclcpp::ok()) {
        RCLCPP_ERROR(this->get_logger(), "Interrupted while waiting for the service. Exiting.");
        return;
      }
      RCLCPP_INFO(this->get_logger(), "service not available, waiting again...");
    }
    RCLCPP_INFO(this->get_logger(), "Parameter Client Available");

//    auto set_default_parameters = this->parameters_client_->set_parameters({
//       rclcpp::Parameter("foo", 2),
//       rclcpp::Parameter("bar", "hello"),
//       rclcpp::Parameter("baz", 1.45),
//       rclcpp::Parameter("foobar", true),
//       rclcpp::Parameter("foobarbaz", std::vector<bool>({true, false})),
//       rclcpp::Parameter("toto", std::vector<uint8_t>({0xff, 0x7f})),
//     });
//
//    for (auto & result : set_default_parameters) {
//      if (!result.successful) {
//        RCLCPP_ERROR(this->get_logger(), "Failed to set parameter: %s", result.reason.c_str());
//      }
//  }


    // Get a few of the parameters just set.
//    for (auto & parameter : parameters_client_->get_parameters({"foo", "baz", "foobarbaz", "toto"})) {
//      std::cout<< "\nParameter name: " << parameter.get_name()<<std::endl;
//      std::cout << "\nParameter value (" << parameter.get_type_name() << "): " <<
//         parameter.value_to_string()<<std::endl;
//    }

}

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<global_planner::GlobalPlannerROS>();
  RCLCPP_INFO(node->get_logger(), "Global Planner Running");
  node->parameters_client_ = std::make_shared<rclcpp::SyncParametersClient>(node);
  node->load_parameters();
  RCLCPP_INFO(node->get_logger(), "Parameters Loaded");
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
