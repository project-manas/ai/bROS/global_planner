//
// Created by Shrijit Singh on 03/11/18.
//

#include "global_planner/global_planner.h"

using namespace global_planner;
using std::placeholders::_1;

void GlobalPlanner::create_global_planner(rclcpp::Node::SharedPtr node,
                                          std::shared_ptr<global_planner::GlobalPlanner> plugin) {
  this->node_ = std::move(node);
  this->plugin_ = plugin;
  plugin->initialize_plugin((rclcpp::Node::SharedPtr) this->node_);
  costmap_subscription_ = this->node_->create_subscription<nav_msgs::msg::OccupancyGrid>("costmap", 1, std::bind(&GlobalPlanner::update_costmap, this, _1));
  goal_subscription_ = this->node_->create_subscription<geometry_msgs::msg::PoseStamped>("move_base_simple/goal", 1, std::bind(&GlobalPlanner::set_goal, this, _1));
  path_publisher_ = this->node_->create_publisher<nav_msgs::msg::Path>("global_path", 10);

}

void GlobalPlanner::set_goal(const geometry_msgs::msg::PoseStamped::SharedPtr goal) {
  RCLCPP_INFO(this->node_->get_logger(), "Goal Set");
  this->goal_ = *goal;
  this->goal_set = true;
}

void GlobalPlanner::update_costmap(const nav_msgs::msg::OccupancyGrid::SharedPtr map){
  if (this->goal_set) {
    RCLCPP_INFO(this->node_->get_logger(), "Received Costmap");

//    this->goal_set = false;
    this->map_ = map;
    nav_msgs::msg::Path path = this->plugin_->create_path_to_pose(this->map_, this->goal_);
    path.header.stamp = this->node_->now();
    path.header.frame_id = "map";
    this->path_publisher_->publish(path);

  }
}





