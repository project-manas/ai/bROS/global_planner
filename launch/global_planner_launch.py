
import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():
    return launch.LaunchDescription([
        launch_ros.actions.Node(package='global_planner', node_executable='global_planner_node', output='screen'),
        launch_ros.actions.Node(package='global_planner', node_executable='publisher_random_maze.py', output='screen'),
    ])