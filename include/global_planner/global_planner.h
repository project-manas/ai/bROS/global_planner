//
// Created by Shrijit Singh on 03/11/18.
//

#ifndef GLOBAL_PLANNER_H
#define GLOBAL_PLANNER_H

#include "rclcpp/rclcpp.hpp"
#include "nav_msgs/msg/occupancy_grid.hpp"
#include "nav_msgs/msg/path.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"

namespace global_planner {
class GlobalPlanner {
 public:
  bool goal_set = false;

  GlobalPlanner() = default;
  virtual ~GlobalPlanner() = default;
  void create_global_planner(rclcpp::Node::SharedPtr node, std::shared_ptr<global_planner::GlobalPlanner> plugin);

 protected:
  virtual void initialize_plugin(rclcpp::Node::SharedPtr node) {};
  virtual nav_msgs::msg::Path create_path_to_pose(nav_msgs::msg::OccupancyGrid::SharedPtr map, geometry_msgs::msg::PoseStamped goal) = 0;

 private:
  rclcpp::Node::SharedPtr node_;
  std::shared_ptr<global_planner::GlobalPlanner> plugin_;
  nav_msgs::msg::OccupancyGrid::SharedPtr map_;
  geometry_msgs::msg::PoseStamped goal_;
  rclcpp::Subscription<nav_msgs::msg::OccupancyGrid>::SharedPtr costmap_subscription_;
  rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr goal_subscription_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr path_publisher_;

  void set_goal(geometry_msgs::msg::PoseStamped::SharedPtr goal);
  void update_costmap(nav_msgs::msg::OccupancyGrid::SharedPtr map);
};
}

#endif //GLOBAL_PLANNER_H
