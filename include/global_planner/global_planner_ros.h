//
// Created by Shrijit Singh on 2018-12-06.
//

#ifndef GLOBAL_PLANNER_GLOBAL_PLANNER_ROS_H
#define GLOBAL_PLANNER_GLOBAL_PLANNER_ROS_H

#include <memory>
#include <sstream>
#include <vector>

#include "rclcpp/rclcpp.hpp"
#include "global_planner/global_planner.h"
#include <pluginlib/class_loader.hpp>

namespace global_planner {
class GlobalPlannerROS : public rclcpp::Node {
 public:
  GlobalPlannerROS();
  pluginlib::ClassLoader<global_planner::GlobalPlanner> plugin_loader_;
  std::shared_ptr<global_planner::GlobalPlanner> plugin_;
  std::shared_ptr<rclcpp::SyncParametersClient> parameters_client_;
  void load_parameters();
};
}

#endif //GLOBAL_PLANNER_GLOBAL_PLANNER_ROS_H
