//
// Created by Shrijit Singh on 2020-02-03.
//

#ifndef GLOBAL_PLANNER_INCLUDE_GLOBAL_PLANNER_RRT_H_
#define GLOBAL_PLANNER_INCLUDE_GLOBAL_PLANNER_RRT_H_

#include <queue>
#include <random>


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>

#include "rclcpp/rclcpp.hpp"
#include "global_planner/global_planner.h"
#include "map_grid/map_grid.h"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "geometry_msgs/msg/point.hpp"
#include "nav_msgs/msg/occupancy_grid.hpp"
#include "nav_msgs/msg/path.hpp"
#include "visualization_msgs/msg/marker.hpp"

namespace rrt {

struct Edge {
  double dist;
};

struct Vertex {
  int x, y;
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, Vertex, Edge> graph_t;
typedef boost::graph_traits<graph_t>::vertex_descriptor vertex_t;
typedef boost::graph_traits<graph_t>::edge_descriptor edge_t;


class MyVisitor : public boost::default_dfs_visitor {
 public:
  MyVisitor() : vv(new std::vector<vertex_t>()) {}

  void discover_vertex(int v, const graph_t &g) const {
    vv->push_back(v);
    return;
  }

  std::vector<vertex_t> &GetVector() const { return *vv; }

 private:
  boost::shared_ptr<std::vector<vertex_t> > vv;
};

class RRT : public global_planner::GlobalPlanner {
 private:
  rclcpp::Node::SharedPtr node;
  shared_ptr<MapGrid> map_grid_;
  nav_msgs::msg::Path *path;
  vertex_t current_, goal_;
  int max_vertices_;
  int max_increment_;
  graph_t graph_;

  std::mt19937 *rng_;
  rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr search_points_publisher_;
  rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr search_lines_publisher_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr path_publisher_;

  void compute_path(nav_msgs::msg::OccupancyGrid::SharedPtr map, geometry_msgs::msg::PoseStamped goal);
  void set_current(int x, int y);
  void set_goal(geometry_msgs::msg::PoseStamped goal);
  vertex_t gen_rand_vertex();
  double euclid_dist(vertex_t u, vertex_t v);
  void publish_search();
  void publish_path();
  std::pair<rrt::vertex_t, double> closest_vertex(vertex_t v);
  bool reached_goal(vertex_t v);
  bool bresenham(vertex_t u, vertex_t v);
  void load_params();

    protected:
  void initialize_plugin(rclcpp::Node::SharedPtr node) override;
  nav_msgs::msg::Path create_path_to_pose(nav_msgs::msg::OccupancyGrid::SharedPtr map, geometry_msgs::msg::PoseStamped goal) override;
};
}


#endif //GLOBAL_PLANNER_INCLUDE_GLOBAL_PLANNER_RRT_H_
