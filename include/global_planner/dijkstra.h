//
// Created by Shrijit Singh on 2018-12-03.
//

#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <queue>

#include "rclcpp/rclcpp.hpp"
#include "global_planner/global_planner.h"
#include "map_grid/map_grid.h"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/occupancy_grid.hpp"
#include "nav_msgs/msg/path.hpp"

namespace dijkstra {
struct CompareDistance {
  bool operator()(MapCell *const &c1, MapCell *const &c2) {
    return c1->distance > c2->distance;
  }
};

class Dijkstra : public global_planner::GlobalPlanner {
 private:
  rclcpp::Node::SharedPtr node;
  shared_ptr<MapGrid> map;
  nav_msgs::msg::Path *path;
  MapCell *current;
  MapCell *goal;
  std::priority_queue<MapCell *, vector<MapCell *>, CompareDistance> unvisited_cells_;
  rclcpp::Publisher<nav_msgs::msg::OccupancyGrid>::SharedPtr potential_publisher_;
  bool allow_unknown_;
  bool smoothen_path_;

  void compute_path(nav_msgs::msg::OccupancyGrid::SharedPtr map, geometry_msgs::msg::PoseStamped goal);
  void set_current(int x, int y);
  void set_goal(geometry_msgs::msg::PoseStamped goal);
  void calculate_potential();
  void update_cell(MapCell *cell, MapCell *current);
  void publish_potential();
  nav_msgs::msg::Path *create_path();
  bool los(MapCell *c1, MapCell *c2, double &trav_cost);


 protected:
  void initialize_plugin(rclcpp::Node::SharedPtr node) override;
  nav_msgs::msg::Path create_path_to_pose(nav_msgs::msg::OccupancyGrid::SharedPtr map, geometry_msgs::msg::PoseStamped goal) override;
};
}

#endif //DIJKSTRA_H
