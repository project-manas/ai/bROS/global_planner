//
// Created by Shrijit Singh on 05/11/18.
//

#ifndef COSTMAP_MAPGRID_H
#define COSTMAP_MAPGRID_H

#include <memory>

#include "nav_msgs/msg/occupancy_grid.hpp"
#include "nav_msgs/msg/path.hpp"
#include "map_cell.h"

using namespace std;

class MapGrid {

 public:
  MapCell **data;
  float resolution;
  unsigned int width;
  unsigned int height;

  static shared_ptr<MapGrid> convert_occupancy_grid_to_map_grid(const nav_msgs::msg::OccupancyGrid::SharedPtr map) {
    shared_ptr<MapGrid>map_grid (new MapGrid);
    map_grid->width = map->info.width;
    map_grid->height = map->info.height;
    map_grid->resolution = map->info.resolution;
    map_grid->data = new MapCell*[map_grid->height];

    auto *cells = new MapCell[map_grid->height * map_grid->width];

    int x, y;
    for (int i = 0; i < map_grid->height * map_grid->width; ++i){
      x = i / map_grid->width;
      y = i % map_grid->width;
      cells[i] = MapCell(x, y);
      cells[i].cost = map->data[i];
    }

    map_grid->data[0] = cells;
    for (int i = 1; i < map_grid->height; ++i) {
      map_grid->data[i] = &cells[map_grid->width * i];
    }

    return map_grid;
  };

  MapCell cell_at(double x, double y) {
    int x_index = x / resolution, y_index = y / resolution;

    if (x_index < width && y_index < height)
      return data[y_index][x_index];

    printf("Out of Bounds!! (%f, %f))\n", x, y);
    MapCell c;
    c.cost = LETHAL_COST;
    return c;
  }

};

#endif //COSTMAP_MAPGRID_H
