//
// Created by Shrijit Singh on 2018-12-03.
//

#ifndef GLOBAL_PLANNER_MAP_CELL_H
#define GLOBAL_PLANNER_MAP_CELL_H

#include <climits>

#define LETHAL_COST 100
#define FREE_SPACE_COST 0

using namespace std;

class MapCell {
 public:
  int x, y;
  int cost;
  double distance;
  bool visited;
  MapCell *predecessor;

  MapCell() = default;
  ~MapCell() = default;
  MapCell(int x, int y) {
      this->x = x;
      this->y = y;
      this->distance = 1000000;
      this->cost = FREE_SPACE_COST;
      this->visited = false;
      this->predecessor = NULL;
  };

};

#endif //GLOBAL_PLANNER_MAP_CELL_H


