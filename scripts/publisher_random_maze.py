# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from time import sleep

import rclpy

from nav_msgs.msg import OccupancyGrid

import numpy as np
from numpy.random import random_integers as rnd
import matplotlib.pyplot as plt

def maze(width=100, height=100, complexity=.01, density =.02):
    # Only odd shapes
    shape = ((height//2)*2+1, (width//2)*2+1)
    # Adjust complexity and density relative to maze size
    complexity = int(complexity*(5*(shape[0]+shape[1])))
    density    = int(density*(shape[0]//2*shape[1]//2))
    # Build actual maze
    Z = np.zeros(shape, dtype=np.uint8)
    # Fill borders
    Z[0,:] = Z[-1,:] = 0
    Z[:,0] = Z[:,-1] = 0
    # Make isles
    for i in range(density):
        x, y = rnd(0,shape[1]//2)*2, rnd(0,shape[0]//2)*2
        Z[y,x] = 100
        for j in range(complexity):
            neighbours = []
            if x > 1:           neighbours.append( (y,x-2) )
            if x < shape[1]-2:  neighbours.append( (y,x+2) )
            if y > 1:           neighbours.append( (y-2,x) )
            if y < shape[0]-2:  neighbours.append( (y+2,x) )
            if len(neighbours):
                y_,x_ = neighbours[rnd(0,len(neighbours)-1)]
                if Z[y_,x_] == 0:
                    Z[y_,x_] = 100
                    Z[y_+(y-y_)//2, x_+(x-x_)//2] = 100
                    x, y = x_, y_
    return Z


# We do not recommend this style as ROS 2 provides timers for this purpose,
# and it is recommended that all nodes call a variation of spin.
# This example is only included for completeness because it is similar to examples in ROS 1.
# For periodic publication please see the other examples using timers.


def main(args=None):
    rclpy.init(args=args)
    # plt.figure(figsize=(10,5))
    # plt.imshow(maze(80,40),cmap=plt.cm.binary,interpolation='nearest')
    # plt.xticks([]),plt.yticks([])
    # plt.show()

    node = rclpy.create_node('costmap')

    publisher = node.create_publisher(OccupancyGrid, 'costmap')

    msg = OccupancyGrid()

    arr = maze(100, 100);
    # arr = [0 for i in range(0, 10000)]
    #
    # for i in range(40, 70):
    #     for j in range(40, 70):
    #          arr[j * 100 + i] = 100
    #
    # for i in range(20, 40):
    #     for j in range(20, 30):
    #         arr[j * 100 + i] = 100
    #
    # for i in range(70, 60):
    #     for j in range(50, 70):
    #         arr[j * 100 + i] = 100
    #
    # for i in range(15, 25):
    #     for j in range(5, 15):
    #         arr[j * 100 + i] = 100


    msg.data = arr.flatten().tolist()
    msg.header.frame_id = "map"
    msg.info.resolution = 1.0
    msg.info.width = 101
    msg.info.height = 101

    while rclpy.ok():
        node.get_logger().info('Publishing Costmap: "%s"' % msg.info)
        publisher.publish(msg)
        sleep(1)

# Destroy the node explicitly
# (optional - otherwise it will be done automatically
# when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
