# **bROS**
bROS is a modular navigation stack built by Project MANAS. It is a complete replacement for the move_base package in ROS along with many performance improvements and addition of many features to ROS.
bROS is targeted mainly at ROS 2 so that it is futureproof and inline with the latest new technologies. Compatibility with ROS 1 may be added later.

### **Planned Features**
---
1. CUDA accelerated RViz
2. Planning   
   a. Move base   
   b. Planner (local, global)   
   c. Costmap  
   d. Grid   
   e. Recovery  
   f. Custom conditional behaviours   
   g. Localization   
   h. Mapping   
   i. Reactive navigation  
3. Perception   
   a. Data fusion nodes
4. Revamp rqt
5. Health monitor plugin
6. Better debugging
7. ML integration
8. Bagfile to Pandas dataset or TF record
9. Sensor management and calibration
10. Sanity checks
11. Auto tune ukf on bagfile
12. Optional subscriptions
13. Integration with OpenAI tools
14. Dependency based launching (see systemd)

# **Global Planner**
Currently supported algorithms:
1. RRT (Default)
2. Dijkstra (Under Progress)


Topics published:   
map_topic (default: /costmap): Costmap (type: nav_msgs/OccupancyGrid)   
sample_points_topic (default: /search_points): Sampled points (type: visualization_msgs/Marker)   
connected_lines_topic (default: /search_lines): Connected points (type: visualization_msgs/Marker)    
map_topic (default: /path): Path (type: nav_msgs/Path)    

### **Getting Started**
---
```sh
cd ~/ros2_ws/src   
git clone https://gitlab.com//project-manas/ai/bROS/global_planner.git
cd ../
colcon build
source ./install/setup.bash
```

Terminal 1:
```sh
cd ~/ros2_ws/src/global_planner/scripts/
python3 publisher_random_maze.py
```

Terminal 2:
```sh
ros2 run global_planner global_planner_node
```

Terminal 3:
```sh
rviz2
```
Give a goal inside the costmap using rviz

